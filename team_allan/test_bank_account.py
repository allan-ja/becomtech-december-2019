from .bank_account import BankAccount

def test_a_new_account_should_have_0_in_balance():
    assert BankAccount().balance == 0


def test_should_retrieve_newly_made_deposits():
    account = BankAccount()
    account.make_deposit(date='21-02-1902', amount=20)
    account.make_deposit(date='28-02-2002', amount=80)
    account.make_deposit(date='05-03-2052', amount=340)
    assert account.statement == [
        {
            'date': '05-03-2052',
            'type': 'deposit',
            'amount': 340,
            'balance': 440
        },
        {
            'date': '28-02-2002',
            'type': 'deposit',
            'amount': 80,
            'balance': 100
        },
        {
            'date': '21-02-1902',
            'type': 'deposit',
            'amount': 20,
            'balance': 20
        }
    ]


def test_should_retrieve_newly_made_withdrawal():
    account = BankAccount()
    account.make_withdrawal(date='21-02-1972', amount=75)
    account.make_withdrawal(date='13-01-2022', amount=100)
    account.make_withdrawal(date='08-09-2024', amount=34)
    assert account.statement == [
        {
            'date': '08-09-2024',
            'type': 'withdrawal',
            'amount': 34,
            'balance': -209
        },
        {
            'date': '13-01-2022',
            'type': 'withdrawal',
            'amount': 100,
            'balance': -175
        },
        {
            'date': '21-02-1972',
            'type': 'withdrawal',
            'amount': 75,
            'balance': -75
        }
    ]


def test_should_retrieve_newly_made_deposits_and_withdrawal():
    account = BankAccount()
    account.make_deposit(date='10-01-2012', amount=1000)
    account.make_deposit(date='13-01-2012', amount=2000)
    account.make_withdrawal(date='14-01-2012', amount=500)
    assert account.statement == [
        {
            'date': '14-01-2012',
            'type': 'withdrawal',
            'amount': 500,
            'balance': 2500
        },
        {
            'date': '13-01-2012',
            'type': 'deposit',
            'amount': 2000,
            'balance': 3000
        },
        {
            'date': '10-01-2012',
            'type': 'deposit',
            'amount': 1000,
            'balance': 1000
        }
    ]