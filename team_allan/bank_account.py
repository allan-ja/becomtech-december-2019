class BankAccount():
    def __init__(self):
        self.transactions = []

    def make_deposit(self, date, amount):
        self.transactions.append({
            'date': date,
            'type': 'deposit',
            'amount': amount,
            'balance': self.balance + amount
            }
        )

    def make_withdrawal(self, date, amount):
        self.transactions.append({
            'date': date,
            'type': 'withdrawal',
            'amount': amount,
            'balance': self.balance - amount
            }
        )

    @property
    def balance(self):
        balance = sum(map(
            lambda x: x['amount'] if x['type'] == 'deposit' else -x['amount'],
            self.transactions))
        return balance

    @property
    def statement(self):
        return [item for item in reversed(self.transactions)]
